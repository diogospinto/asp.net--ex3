Create Table appuser (
	username Varchar(25) Constraint pk_appuser_username Primary Key, 
	first_name Varchar(50) Constraint nn_appuser_first_name Not Null, 
	last_name Varchar(50) Constraint nn_appuser_last_name Not Null, 
	pwd Varchar(50) Constraint nn_appuser_pwd Not Null, 
	administrator Varchar(1) Constraint nn_appuser_administrator Not Null, 
	permission Int Constraint fk_appuser_permission_level Foreign Key References permission_level(id), 
	account_state Varchar(8) Constraint nn_appuser_account_state Not Null, 
	approved_admin Varchar(25)
)

Go

Select *
From appuser

Delete From appuser
Where username = 'misc'

Select *
From account_state

Select *
From permission_level

Go

Alter Table appuser
Alter Column account_state Int

Go

Alter Table appuser
Add Constraint fk_appuser_account_state Foreign Key(account_state) References account_state(id)

Go

Create Table account_state(
	id Int Identity(1,1) Constraint pk_account_state_id Primary Key, 
	account_state Varchar(8) Constraint nn_account_state_account_state Not Null
)

Go

Insert Into account_state (account_state) 
	Values ('approved'), 
		   ('pending')

Go

--Select len('approved') -- 8

--Select len('pending') -- 7

Create Table permission_level (
	id Int Identity(1,1) Constraint pk_permission_level_id Primary Key, 
	permission_value Varchar(13) Constraint nn_permission_level_permission_value Not Null
)

Go

Insert Into permission_level (permission_value)
	Values ('leitor'), 
		  ('escritor'), 
		  ('revisor'), 
		  ('administrador')

Go

Create Table categories (
	category_id Int Identity(1,1) Constraint pk_categories_id Primary Key,
	title Varchar(60) Constraint nn_categories_title Not Null 
					  Constraint uk_categories_title Unique, 
	subtitle Varchar(200), 
	approver_user Varchar(25) Constraint fk_categories_approver_user Foreign Key References appuser(username),
	approved_date DateTime, 
	published Varchar(1) Constraint nn_categories_published Not Null, 
	publisher Varchar(25) Constraint nn_categories_publisher Not Null 
						  Constraint fk_categories_published Foreign Key References appuser(username), 
	published_date DateTime Constraint nn_categories_published_date Not Null, 
	remover Varchar(25) Constraint fk_categories_remover Foreign Key References appuser(username), 
	removed_date DateTime
)

Select *
From categories

Go

Create Table articles (
	article_id Int Identity Constraint pk_articles_id Primary Key, 
	title Varchar(60) Constraint nn_articles_title Not Null 
					  Constraint uk_articles_title Unique, 
	subtitle Varchar(200), 
	content Varchar(Max) Constraint nn_articles_content Not Null, 
	creator Varchar(25) Constraint nn_articles_creator Not Null 
						Constraint fk_articles_creator Foreign Key References appuser(username), 
	created_date DateTime Constraint nn_articles_created_date Not Null, 
	approver_user Varchar(25) Constraint fk_articles_approver_user Foreign Key References appuser(username), 
	approved_date DateTime, 
	published Varchar(1) Constraint nn_articles_published Not Null, 
	publisher Varchar(25) Constraint nn_articles_publisher Not Null
						  Constraint fk_articles_publisher Foreign Key References appuser(username), 
	published_date DateTime Constraint nn_articles_published_date Not Null, 
	remover Varchar(25) Constraint fk_articles_remover Foreign Key References appuser(username), 
	removed_date DateTime
)

Insert Into articles (title, subtitle, content, creator, created_date, published, publisher, published_date) 
	Values('Title 1', 'Subtitle 1', 'Content 1', 'master', GetDate(), 'N', 'master', GetDate())

Go

Create Table categories_articles (
	category_id Int Constraint fk_products_articles_product_id Foreign Key References categories(category_id), 
	article_id Int Constraint fk_products_articles_article_id Foreign Key References articles(article_id), 
	Constraint pk_categories_articles_category_id_article_id Primary Key(category_id, article_id)
)

Go

Create Table article_images (
	article_id Int Constraint fk_article_images_article_id Foreign Key References articles(article_id), 
	article_thumb_image Varchar(Max) Constraint nn_article_images_article_thumb_image Not Null, 
	article_main_image Varchar(Max) Constraint nn_article_images_article_main_image Not Null, 
	Constraint pk_article_images_article_id Primary Key(article_id)
)

Declare @temp Table (
	[id] Int
)
Insert Into [articles] ([title], [subtitle], [content], [creator], [created_date], [published], [publisher], [published_date]) 
	Output inserted.article_id Into @temp
	Values ('Title1', 'Subtitle1', 'Content1', 'mavus', GetDate(), 'S', 'mavus', GetDate())
Select *
From @temp

DBCC Checkident (articles, reseed, 0)

Select *
From articles

Select *
From article_views

Select *
From article_images

Delete From articles

Delete From article_views

Delete From article_images

Go

Create Table article_views (
	article_id Int Constraint fk_article_views_article_id Foreign Key References articles(article_id), 
	viewer_name Varchar(25) Constraint fk_article_views_viewer_name Foreign Key References appuser(username),
	article_view_count Int Constraint nn_article_views_article_view_count Not Null, 
	article_view_date DateTime,
	Constraint pk_article_views_article_id_viewer_name_article_view_date Primary Key(article_id, viewer_name, article_view_date)
)

--Select len('leitor') -- 6

--Select len('escritor') -- 8

--Select len('revisor') -- 7

--Select len('administrador') -- 13

Create Trigger insertIntoArticleViews 
On articles
After Insert
As
Begin
	Set Nocount On
	Insert Into article_views (
		article_id, 
		viewer_name,
		article_view_count, 
		article_view_date
	)
	Select 
		i.article_id,
		i.creator, 
		0, 
		GetDate()
	From
		inserted i
End
Go