﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddCategory.aspx.cs" Inherits="WebApp_Ex3.AddCategory" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Button ID="btnLogout" Text="Logout" OnClick="Logout_Click" runat="server" />

            <ul id="ulLinks">
                <li><asp:HyperLink ID="linkArticleList" Text="Lista de Artigos" NavigateUrl="~/Products.aspx" runat="server"></asp:HyperLink></li>
            </ul>

            <h2>Nova Categoria</h2>

            <asp:TextBox ID="txtBoxTitle" placeholder="Título" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtBoxSubtitle" placeholder="Sub-Título" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button Text="Criar" OnClientClick="return CheckFields();" OnClick="Criar_Click" runat="server" />
            <br />
            <br />
            <span id="infoMsg" runat="server"></span>

        </div>
    </form>

    <script>
        function CheckFields() {
            let title = document.getElementById('<%= txtBoxTitle.ClientID %>').value;
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');

            if (title == "") {
                infoMsg.innerHTML = "O campo <i>'Título'</i> é obrigatório! ";
                return false;
            } else {
                return true;
            }
        }
    </script>
</body>
</html>
