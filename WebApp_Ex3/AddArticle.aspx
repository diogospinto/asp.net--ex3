﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddArticle.aspx.cs" Inherits="WebApp_Ex3.AddArticle" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Button ID="btnLogout" Text="Logout" OnClick="Logout_Click" runat="server" />

            <ul id="ulLinks">
                <li><asp:HyperLink ID="linkArticleList" Text="Lista de Artigos" NavigateUrl="~/Products.aspx" runat="server"></asp:HyperLink></li>
            </ul>

            <h2>Novo Artigo</h2>

            <asp:TextBox ID="txtBoxTitle" placeholder="Título" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtBoxSubTitle" placeholder="Sub-Título" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox TextMode="MultiLine" ID="txtBoxContent" Rows="6" Columns="50" Style="resize: none" placeholder="Conteúdo" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="lblUploadThumbnail" Text="Upload Thumbnail" runat="server"></asp:Label>
            <br />
            <asp:FileUpload ID="uploadThumbnail" runat="server" />
            <br />
            <br />
            <asp:Label ID="lblUploadMainImage" Text="Upload Image Principal" runat="server"></asp:Label>
            <br />
            <asp:FileUpload ID="uploadMainImg" runat="server" />
            <br />
            <br />
            <asp:Button Text="Criar" OnClientClick="return CheckFields() && CheckFilesUploaded();" OnClick="Criar_Click" runat="server" />
            <br />
            <br />
            <span id="infoMsg" runat="server"></span>

        </div>
    </form>

    <script>
        function CheckFields() {
            let title = document.getElementById('<%= txtBoxTitle.ClientID %>').value;
            let content = document.getElementById('<%= txtBoxContent.ClientID %>').value;
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');

            if (title == '' || content == '') {
                infoMsg.innerHTML = 'O título e conteúdo do artigo têm que ser preenchidos! ';
                return false;
            } else {
                return true;
            }
        }

        function CheckFilesUploaded() {
            let thumbImage = document.getElementById('<%= uploadThumbnail.ClientID %>').value;
            let mainImage = document.getElementById('<%= uploadMainImg.ClientID %>').value;
            let infoMsg = document.getElementById('<%= infoMsg.ClientID %>');

            if (thumbImage == "" || mainImage == "") {
                infoMsg.innerHTML = 'É necessário fazer upload de ambas as imagens! ';
                return false;
            } else {
                return true;
            }
        }
    </script>
</body>
</html>
