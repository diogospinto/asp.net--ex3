﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ManageAccounts.aspx.cs" Inherits="WebApp_Ex3.ManageAccounts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:Button Text="Logout" OnClick="Logout_Click" runat="server" />

            <h1>Lista de Contas Pendentes</h1>
            <h3 id="statusMsg" runat="server"></h3>

            <ul id="ulLinks" >
                <li><asp:HyperLink ID="linkCreateAdmin" Text="Criar Administrador" NavigateUrl="~/CreateAdministrator.aspx" runat="server"></asp:HyperLink></li>
                <li><asp:HyperLink ID="linkCreateUser" Text="Criar Utilizador" NavigateUrl="~/CreateUser.aspx" runat="server"></asp:HyperLink></li>
                <li><asp:HyperLink ID="linkShowProducts" Text="Lista de Artigos" NavigateUrl="~/Products.aspx" runat="server"></asp:HyperLink></li>
            </ul>

            <span id="pendingAccounts" runat="server"></span>

            <div id="mainUserDiv" runat="server">

            </div>

        </div>
    </form>
</body>
</html>
