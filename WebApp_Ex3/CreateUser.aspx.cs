﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebApp_Ex3
{
    public partial class CreateUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgToRed();
            CheckIfDBHasAdmin();
        }

        protected void CreateUser_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();

            if (!CheckFields())
            {
                errorMsg.InnerText = "Pf preencher todos os campos! ";
            } else if (!CheckIfPasswordsMatch())
            {
                errorMsg.InnerText = "Passwords não são iguais! ";
            } else if (dal.CheckIfUserExists(txtBoxUsername.Text))
            {
                errorMsg.InnerText = "Username já existe! ";
            } else
            {
                User u = new User()
                {
                    Username = txtBoxUsername.Text,
                    FirstName = txtBoxFirstName.Text,
                    LastName = txtBoxLastName.Text,
                    Password = txtBoxPassword.Text,
                    Administrator = "N",
                    Permission = 1,
                    AccountState = 2
                };

                if (dal.InsertUser(u) == null)
                {
                    errorMsg.InnerText = "Não foi possível criar o utilizador! ";
                } else
                {
                    ClearFields();
                    MsgToGreen();
                    errorMsg.InnerText = "Criado utilizador com sucesso! ";
                }
            }
        }

        public bool CheckFields()
        {
            if (string.IsNullOrEmpty(txtBoxUsername.Text) || string.IsNullOrEmpty(txtBoxFirstName.Text) || string.IsNullOrEmpty(txtBoxLastName.Text) ||
                string.IsNullOrEmpty(txtBoxPassword.Text) || string.IsNullOrEmpty(txtBoxPasswordConfirm.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void ClearFields()
        {
            txtBoxUsername.Text = string.Empty;
            txtBoxFirstName.Text = string.Empty;
            txtBoxLastName.Text = string.Empty;
            txtBoxPassword.Text = string.Empty;
            txtBoxPasswordConfirm.Text = string.Empty;
        }

        public bool CheckIfPasswordsMatch()
        {
            if (txtBoxPassword.Text == txtBoxPasswordConfirm.Text)
            {
                return true;
            } else
            {
                return false;
            }
        }

        protected void CheckIfDBHasAdmin()
        {
            DAL dal = new DAL();

            if (!dal.CheckIfDbHasAdministrator())
            {
                Response.Redirect("LoginPage.aspx");
            }
        }

        protected void MsgToRed()
        {
            errorMsg.Style.Clear();
            errorMsg.Style.Add("color", "red");
        }

        protected void MsgToGreen()
        {
            errorMsg.Style.Clear();
            errorMsg.Style.Add("color", "green");
        }
    }
}