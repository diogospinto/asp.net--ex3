﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace WebApp_Ex3
{
    public partial class ManageAccounts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckIfUserLoggedIn();
            DisplayPendingAccounts();
        }

        protected void CheckIfUserLoggedIn()
        {
            if (Session["username"] == null)
            {
                Response.Redirect("LoginPage.aspx");
            }
        }

        protected void DisplayPendingAccounts()
        {
            DAL dal = new DAL();

            if (dal.CheckIfPendingAccountsExist() == null)
            {
                StatusMsgToGreen(pendingAccounts);
                pendingAccounts.InnerHtml = "<strong>Não existem contas pendentes.</strong>";
            } else
            {
                List<User> listPendingAccounts = dal.CheckIfPendingAccountsExist();

                pendingAccounts.InnerHtml = 
                    $"{(listPendingAccounts.Count > 1 ? $"<h4>Existem {listPendingAccounts.Count} contas pendentes.</h4>" : $"<h4>Existe {listPendingAccounts.Count} conta pendente. </h4>")}";

                
                StatusMsgToRed(pendingAccounts);

                foreach (User pendingAccountUser in listPendingAccounts)
                {
                    HtmlGenericControl userDiv = CreateDiv(pendingAccountUser.Username);

                    CreateUsernameParagraph(pendingAccountUser.Username, userDiv);

                    CreateNameParagraph(pendingAccountUser.Username, pendingAccountUser.FirstName, pendingAccountUser.LastName, userDiv);

                    CreatePermissionParagraph(pendingAccountUser.Username, pendingAccountUser.Permission, userDiv);

                    CreateApproveButton(pendingAccountUser.Username, userDiv);
                }
            }
        }

        protected void btnApprove_click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            string btnText = btn.Text;
            string[] username = btnText.Split();

            User u = new User()
            {
                Username = username[1], 
                AccountState = 1, 
                ApprovedAdmin = Session["username"].ToString()
            };

            DAL dal = new DAL();

            if (dal.UpdateUserAccountState(u))
            {
                StatusMsgToGreen(statusMsg);
                Response.Redirect(Request.RawUrl);
                statusMsg.InnerHtml = $"Aprovado '{username[1]}' com sucesso! ";
            } else
            {
                StatusMsgToRed(statusMsg);
                statusMsg.InnerHtml = $"Não foi possível aprovar utilizador '${username[1]}'";
            }
        }

        protected HtmlGenericControl CreateDiv(string username)
        {
            HtmlGenericControl userDiv = new HtmlGenericControl("div");
            userDiv.Attributes.Add("id", $"div_{username}");
            mainUserDiv.Controls.Add(userDiv);

            return userDiv;
        }

        protected void CreateUsernameParagraph(string username, HtmlGenericControl div)
        {
            HtmlGenericControl usernameParagraph = new HtmlGenericControl("p");
            usernameParagraph.Attributes.Add("id", $"username_{username}");
            usernameParagraph.InnerHtml = $"<strong>Username:</strong> {username}<br>";
            div.Controls.Add(usernameParagraph);
        }

        protected void CreateNameParagraph(string username, string firstName, string lastName, HtmlGenericControl div)
        {
            HtmlGenericControl nameParagraph = new HtmlGenericControl("p");
            nameParagraph.Attributes.Add("id", $"name_{username}");
            nameParagraph.InnerHtml = $"<strong>Nome:</strong> {firstName} {lastName}";
            div.Controls.Add(nameParagraph);
        }

        protected void CreatePermissionParagraph(string username, int permission, HtmlGenericControl div)
        {
            HtmlGenericControl permissionParagraph = new HtmlGenericControl("p");
            permissionParagraph.Attributes.Add("id", $"permission_{username}");
            permissionParagraph.InnerHtml = $"<strong>Permissão:</strong> {(permission == 1 ? "Leitor" : "Outra")}";
            div.Controls.Add(permissionParagraph);
        }

        protected void CreateApproveButton(string username, HtmlGenericControl div)
        {
            Button btnApprove = new Button();
            btnApprove.Text = $"Aprovar {username}";
            btnApprove.ID = $"btnApprove_{username}";
            btnApprove.Click += new EventHandler(btnApprove_click);
            div.Controls.Add(btnApprove);
        }

        protected void StatusMsgToGreen(HtmlGenericControl field)
        {
            field.Style.Clear();
            field.Style.Add("color", "green");
        }

        protected void StatusMsgToRed(HtmlGenericControl field)
        {
            field.Style.Clear();
            field.Style.Add("color", "red");
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginPage.aspx");
        }
    }
}