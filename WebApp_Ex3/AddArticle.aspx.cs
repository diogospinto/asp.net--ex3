﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp_Ex3.Classes;
using System.Drawing;
using System.IO;

namespace WebApp_Ex3
{
    public partial class AddArticle : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckIfUserIsLoggedIn();
            PopulateCategories();
            MsgToRed();
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginPage.aspx");
        }

        protected void CheckIfUserIsLoggedIn()
        {
            if (Session["username"] == null)
            {
                Response.Redirect("LoginPage.aspx");
            }
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            if (!CheckFields())
            {
                infoMsg.InnerHtml = "O título e conteúdo do artigo têm que ser preenchidos! ";
            } else if (!CheckImagesUpload())
            {
                infoMsg.InnerHtml = "É necessário fazer upload de ambas as imagens! ";
            } else
            {
                Article article = new Article()
                {
                    Title = txtBoxTitle.Text,
                    SubTitle = txtBoxSubTitle.Text,
                    Content = txtBoxContent.Text,
                    Creator = Session["username"].ToString(),
                    CreatedDate = DateTime.Now,
                    Published = "S",
                    Publisher = Session["username"].ToString(),
                    PublishedDate = DateTime.Now, 
                    ThumbImage = GetImagePath(uploadThumbnail, "Thumb"), 
                    MainImage = GetImagePath(uploadMainImg, "Main")
                };

                DAL dal = new DAL();

                if (dal.CheckIfArticleTitleExists(article.Title))
                {
                    infoMsg.InnerHtml = "Título já existe! ";
                } else
                {
                    if (dal.CreateArticle(article) != null)
                    {
                        Article newArticle = dal.GetArticleID(article.Title);

                        article.ID = newArticle.ID;

                        if (dal.InsertArticleImages(article) != null)
                        {
                            MsgToGreen();
                            infoMsg.InnerHtml = "Artigo Criado com sucesso! ";
                        } else
                        {
                            infoMsg.InnerHtml = "Não foi possível guardar imagens! ";
                        }
                    }
                    else
                    {
                        infoMsg.InnerHtml = "Não foi possível criar artigo! ";
                    }
                }
            }
        }

        protected bool CheckFields()
        {
            return (txtBoxTitle.Text == string.Empty || txtBoxContent.Text == string.Empty) ? false : true;
        }

        protected bool CheckImagesUpload()
        {
            return (!uploadThumbnail.HasFile || !uploadMainImg.HasFile) ? false : true;
        }

        protected string GetImagePath(FileUpload fileUpload, string thumbOrMain)
        {
            if (fileUpload.HasFile)
            {
                string altTitle = ReplaceCharacters(txtBoxTitle.Text);

                string fileName = $"{altTitle}_{Session["username"].ToString()}_{thumbOrMain}_{Path.GetExtension(fileUpload.FileName)}";
                string folderName = Path.Combine(Server.MapPath(@"\Images"), Session["username"].ToString());

                if (!Directory.Exists(folderName))
                {
                    Directory.CreateDirectory(folderName);
                }

                string pathString = Path.Combine(folderName, fileName);

                if (!File. Exists(pathString))
                {
                    fileUpload.SaveAs(pathString);

                    return pathString;
                } else
                {
                    int cont = 0;
                    string newFileName = fileName;
                    
                    while (File.Exists(newFileName))
                    {
                        ++cont;
                        newFileName = $"({cont.ToString()}){fileName}";
                    }

                    pathString = Path.Combine(folderName, newFileName);

                    fileUpload.SaveAs(pathString);

                    return pathString;
                }

            } else
            {
                return string.Empty;
            }
        }

        protected string ReplaceCharacters(string fileName)
        {
            fileName = fileName.Replace(" ", "_");
            fileName = fileName.Replace(":", "_");
            fileName = fileName.Replace("~", "_");
            fileName = fileName.Replace("#", "_");
            fileName = fileName.Replace("%", "_");
            fileName = fileName.Replace("&", "_");
            fileName = fileName.Replace("*", "_");
            fileName = fileName.Replace("(", "_");
            fileName = fileName.Replace(")", "_");
            fileName = fileName.Replace("{", "_");
            fileName = fileName.Replace("}", "_");
            fileName = fileName.Replace("[", "_");
            fileName = fileName.Replace("]", "_");
            fileName = fileName.Replace("\\", "_");
            fileName = fileName.Replace("<", "_");
            fileName = fileName.Replace(">", "_");
            fileName = fileName.Replace("?", "_");
            fileName = fileName.Replace("/", "_");
            fileName = fileName.Replace("|", "_");
            fileName = fileName.Replace("\"", "_");

            return fileName;
        }

        protected List<Category> PopulateCategories()
        {
            DAL dal = new DAL();

            List<Category> listCategories = dal.GetListOfCategories();

            if (listCategories == null)
            {
                infoMsg.InnerHtml = "Sem categorias para apresentar! ";
                return null;
            } else
            {
                infoMsg.InnerHtml = "Categorias: <br>";
                foreach (Category category in listCategories)
                {

                    infoMsg.InnerHtml += $"{category.Title}<br>";
                }

                return listCategories;
            }
        }

        protected void MsgToGreen()
        {
            infoMsg.Style.Clear();
            infoMsg.Style.Add("color", "green");
        }

        protected void MsgToRed()
        {
            infoMsg.Style.Clear();
            infoMsg.Style.Add("color", "red");
        }
    }
}