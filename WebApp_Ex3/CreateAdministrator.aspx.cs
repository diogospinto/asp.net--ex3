﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp_Ex3
{
    public partial class CreateAdministrator : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgToRed();
            ViewOrHideLogoutButton();
        }

        protected void CriarAdmin_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();

            if (!CheckFields())
            {
                errorMsg.InnerText = "Pf preencher todos os campos! ";
            } else if (!CheckIfPasswordsMatch(txtBoxPassword.Text, txtBoxPasswordConfirm.Text))
            {
                errorMsg.InnerText = "Password não são iguais! ";
            } else if (dal.CheckIfUserExists(txtBoxUsername.Text))
            {
                errorMsg.InnerText = "Username já existe! ";
            } else
            {
                User u = new User()
                {
                    Username = txtBoxUsername.Text,
                    FirstName = txtBoxFirstName.Text,
                    LastName = txtBoxLastName.Text,
                    Password = txtBoxPassword.Text, 
                    Administrator = "S", 
                    Permission = 4, 
                    AccountState = 1
                };

                if (dal.InsertUser(u) == null)
                {
                    errorMsg.InnerText = "Nao foi possível criar o Administrador";
                } else
                {
                    MsgToGreen();
                    errorMsg.InnerText = "Criado Administrador com sucesso! ";
                    ClearFields();
                }
            }
        }

        public bool CheckFields()
        {
            if (string.IsNullOrEmpty(txtBoxUsername.Text) || string.IsNullOrEmpty(txtBoxFirstName.Text) || string.IsNullOrEmpty(txtBoxLastName.Text) || 
                string.IsNullOrEmpty(txtBoxPassword.Text) || string.IsNullOrEmpty(txtBoxPasswordConfirm.Text))
            {
                return false;
            } else
            {
                return true;
            }
        }

        public void ClearFields()
        {
            txtBoxUsername.Text = string.Empty;
            txtBoxFirstName.Text = string.Empty;
            txtBoxLastName.Text = string.Empty;
            txtBoxPassword.Text = string.Empty;
            txtBoxPasswordConfirm.Text = string.Empty;
        }

        public bool CheckIfPasswordsMatch(string pwd, string pwdConfirm)
        {
            if (pwd == pwdConfirm)
            {
                return true;
            } else
            {
                return false;
            }
        }

        protected void VoltarLogin_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoginPage.aspx");
        }

        protected void MsgToRed()
        {
            errorMsg.Style.Clear();
            errorMsg.Style.Add("color", "red");
        }

        protected void MsgToGreen()
        {
            errorMsg.Style.Clear();
            errorMsg.Style.Add("color", "red");
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginPage.aspx");
        }

        protected void ViewOrHideLogoutButton()
        {
            if (Session["username"] == null)
            {
                btnLogout.Visible = false;
            } else
            {
                btnLogout.Visible = true;
            }
        }
    }
}