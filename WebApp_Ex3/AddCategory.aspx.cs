﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp_Ex3.Classes;

namespace WebApp_Ex3
{
    public partial class AddCategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckIfUserIsLoggedIn();
            MsgToRed();
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("LoginPage.aspx");
        }

        protected void CheckIfUserIsLoggedIn()
        {
            if (Session["username"] == null)
            {
                Response.Redirect("LoginPage.aspx");
            }
        }

        protected void Criar_Click(object sender, EventArgs e)
        {
            if (!CheckFields())
            {
                infoMsg.InnerHtml = "O campo 'Título' é obrigatório! ";
            } else
            {
                Category category = new Category()
                {
                    Title = txtBoxTitle.Text, 
                    SubTitle = txtBoxSubtitle.Text, 
                    Published = "S", 
                    Publisher = Session["username"].ToString(),
                    PublishedDate = DateTime.Now
                };

                DAL dal = new DAL();

                if (dal.CreateCategory(category) != null)
                {
                    MsgToGreen();
                    infoMsg.InnerHtml = "Criada categoria com sucesso! ";
                    ClearFields();
                } else
                {
                    infoMsg.InnerHtml = "Não foi possível criar a categoria! ";
                }
            }
        }

        protected void MsgToGreen()
        {
            infoMsg.Style.Clear();
            infoMsg.Style.Add("color", "green");
        }

        protected void MsgToRed()
        {
            infoMsg.Style.Clear();
            infoMsg.Style.Add("color", "red");
        }

        protected bool CheckFields()
        {
            if (txtBoxTitle.Text == string.Empty)
            {
                return false;
            }
            return true;
        }

        protected void ClearFields()
        {
            txtBoxTitle.Text = string.Empty;
            txtBoxSubtitle.Text = string.Empty;
        }
    }
}