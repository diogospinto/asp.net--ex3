﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="WebApp_Ex3.CreateUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Criar Utilizador</h1>
            <asp:TextBox ID="txtBoxUsername" placeholder="Username" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtBoxFirstName" placeholder="Primeiro Nome" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtBoxLastName" placeholder="Último Nome" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtBoxPassword" placeholder="Password" type="password" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtBoxPasswordConfirm" placeholder="Confirmar Password" type="password" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button Text="Criar" OnClientClick="return CheckFields() && CheckPasswordMatch();" OnClick="CreateUser_Click" runat="server" />
            <br />
            <br />
            <asp:HyperLink ID="linkLogin" Text="Login" NavigateUrl="~/LoginPage.aspx" runat="server"></asp:HyperLink>
            <br />
            <br />
            <asp:HyperLink ID="linkCreateAdmin" Text="Criar Administrador" NavigateUrl="~/CreateAdministrator.aspx" runat="server"></asp:HyperLink>
            <br />
            <br />
            <span id="errorMsg" runat="server"></span>
        </div>
    </form>

    <script>
        function CheckFields() {
            let username = document.getElementById('<%= txtBoxUsername.ClientID %>').value;
            let firstName = document.getElementById('<%= txtBoxFirstName.ClientID %>').value;
            let lastName = document.getElementById('<%= txtBoxLastName.ClientID %>').value;
            let password = document.getElementById('<%= txtBoxPassword.ClientID %>').value;
            let passwordConfirm = document.getElementById('<%= txtBoxPasswordConfirm.ClientID %>').value;
            let errorMsg = document.getElementById('<%= errorMsg.ClientID %>');

            if (username == "" || firstName == "" || lastName == "" || password == "" || passwordConfirm == "") {
                errorMsg.innerHTML = 'Pf preencher todos os campos! ';
                return false;
            } else {
                return true;
            }
        }

        function CheckPasswordMatch() {
            let password = document.getElementById('<%= txtBoxPassword.ClientID %>').value;
            let passwordConfirm = document.getElementById('<%= txtBoxPasswordConfirm.ClientID %>').value;
            let errorMsg = document.getElementById('<%= errorMsg.ClientID %>');

            if (password === passwordConfirm) {
                return true;
            } else {
                errorMsg.innerHTML = 'As password não são iguais! ';
                return false;
            }
        }
    </script>
</body>
</html>
