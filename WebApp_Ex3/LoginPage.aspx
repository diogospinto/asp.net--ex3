﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="WebApp_Ex3.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title>WebApp</title>

    

</head>
<body>
    <form id="form1" runat="server">
        <div>

            <h1>Login</h1>
            
            <asp:TextBox ID="txtBoxUsername" placeholder="Username" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:TextBox ID="txtBoxPassword" type="password" placeholder="Password" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button text="Login" OnClientClick="return CheckFields();" OnClick="Login_Click" runat="server"/>
            <br />
            <br />
            <span id="errorMsg" runat="server"></span>
        </div>

        <asp:HyperLink ID="linkCreateAdmin" Text="Criar Administrador" NavigateUrl="~/CreateAdministrator.aspx" runat="server"></asp:HyperLink>
        <br />
        <br />
        <asp:LinkButton ID="linkBtnCreateUser" Text="Criar Utilizador" OnClick="LinkBtnCreateUser_Click" runat="server"></asp:LinkButton>
        <br />
        <br />
        <span id="bottomMsg" runat="server"></span>
    </form>

    <script>
        function CheckFields() {
            let username = document.getElementById('<%= txtBoxUsername.ClientID %>').value;
            let password = document.getElementById('<%= txtBoxPassword.ClientID %>').value;
            let errorMsg = document.getElementById('<%= errorMsg.ClientID %>');

            if (username == "" || password == "") {
                errorMsg.innerHTML = 'Pf preencher todos os campos! ';
                return false;
            } else {
                return true;
            }
        }
    </script>
</body>
</html>
