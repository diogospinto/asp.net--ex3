﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp_Ex3.Classes
{
    public class Category
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Approver_User { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string Published { get; set; }
        public string Publisher { get; set; }
        public DateTime PublishedDate { get; set; }
        public string Remover { get; set; }
        public DateTime? RemovedDate { get; set; }
    }
}