﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;

namespace WebApp_Ex3.Classes
{
    public class Article
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Content { get; set; }
        public string Creator { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Approver { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string Published { get; set; }
        public string Publisher { get; set; }
        public DateTime PublishedDate { get; set; }
        public string Remover { get; set; }
        public DateTime RemovedDate { get; set; }
        public string ThumbImage { get; set; }
        public string MainImage { get; set; }
        public int ViewCount { get; set; }
        public string Viewer { get; set; }
        public DateTime ViewDate { get; set; }
    }
}