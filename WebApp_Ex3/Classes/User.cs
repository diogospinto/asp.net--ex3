﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp_Ex3
{
    public class User
    {
        public string Username { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Password { get; set; }

        public string Administrator { get; set; }

        public int Permission { get; set; }

        public int AccountState { get; set; }

        public string ApprovedAdmin { get; set; }
    }
}