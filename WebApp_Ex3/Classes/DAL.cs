﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using WebApp_Ex3.Classes;

namespace WebApp_Ex3
{
    public class DAL
    {
        private SqlConnection _connection;

        public DAL()
        {
            _connection = new SqlConnection()
            {
                ConnectionString = @"Data Source=WINDOWS10\SQLEXPRESS;Initial Catalog=webappex3;Integrated Security=True;Connect Timeout=30;
                        Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
            };
        }

        public User InsertUser(User u)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Insert Into [appuser] ([username], [first_name], [last_name], [pwd], [administrator], [permission], [account_state]) " +
                    "Values (@username, @first_name, @last_name, @pwd, @administrator, @permission, @account_state)", _connection);

                sqlCommand.Parameters.AddWithValue("@username", u.Username);
                sqlCommand.Parameters.AddWithValue("@first_name", u.FirstName);
                sqlCommand.Parameters.AddWithValue("@last_name", u.LastName);
                sqlCommand.Parameters.AddWithValue("@pwd", EncryptPassword(u.Password));
                sqlCommand.Parameters.AddWithValue("@administrator", u.Administrator);
                sqlCommand.Parameters.AddWithValue("@permission", u.Permission);
                sqlCommand.Parameters.AddWithValue("@account_state", u.AccountState);

                int rows = sqlCommand.ExecuteNonQuery();

                return (rows > 0) ? u : null;

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public User GetUser(User u)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select [username], [pwd] From [appuser] Where [username] = @username And [pwd] = @pwd", _connection);

                sqlCommand.Parameters.AddWithValue("@username", u.Username);
                sqlCommand.Parameters.AddWithValue("@pwd", u.Password);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    dataReader.Close();
                    return u;
                } else
                {
                    dataReader.Close();
                    return null;
                }

            } catch(Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public bool CheckIfDbHasAdministrator()
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select * From [appuser] Where [administrator] Like 'S'", _connection);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    dataReader.Close();
                    return true;
                } else
                {
                    dataReader.Close();
                    return false;
                }

            } catch(Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public bool CheckIfUserExists(string username)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select [username] From [appuser] Where [username] = @username", _connection);

                sqlCommand.Parameters.AddWithValue("@username", username);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    dataReader.Close();
                    return true;
                } else
                {
                    dataReader.Close();
                    return false;
                }

            } catch(Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public User CheckUserAccountState(User u)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select [account_state] From [appuser] Where [username] = @username", _connection);

                sqlCommand.Parameters.AddWithValue("@username", u.Username);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    u.AccountState = int.Parse(dataReader["account_state"].ToString());

                    if (u.AccountState == 2)
                    {
                        return null;
                    } else
                    {
                        return u;
                    }
                } else
                {
                    return null;
                }

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public bool CheckIfUserIsAdmin(User u)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select [permission] From [appuser] Where [username] = @username", _connection);

                sqlCommand.Parameters.AddWithValue("@username", u.Username);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    u.Permission = int.Parse(dataReader["permission"].ToString());

                    if (u.Permission == 4)
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                } else
                {
                    return false;
                }
            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public List<User> CheckIfPendingAccountsExist()
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select * From [appuser] Where [account_state] = 2", _connection);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                List<User> listPendingAccounts = new List<User>();

                while (dataReader.Read())
                {
                    User u = new User()
                    {
                        Username = dataReader["username"].ToString(),
                        FirstName = dataReader["first_name"].ToString(),
                        LastName = dataReader["last_name"].ToString(),
                        Permission = int.Parse(dataReader["permission"].ToString()),
                        AccountState = int.Parse(dataReader["account_state"].ToString())
                    };

                    listPendingAccounts.Add(u);
                }

                dataReader.Close();

                return (listPendingAccounts.Count > 0) ? listPendingAccounts : null;

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public bool UpdateUserAccountState(User u)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Update [appuser]" +
                    "Set [account_state] = @account_state, [approved_admin] = @approved_admin Where [username] = @username", _connection);

                sqlCommand.Parameters.AddWithValue("@account_state", u.AccountState);
                sqlCommand.Parameters.AddWithValue("@approved_admin", u.ApprovedAdmin);
                sqlCommand.Parameters.AddWithValue("@username", u.Username);

                return (sqlCommand.ExecuteNonQuery() > 0) ? true : false;

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public Category CreateCategory(Category category)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Insert Into [categories] ([title], [subtitle], [published], [publisher], [published_date]) " +
                    "Values (@title, @subtitle, @published, @publisher, @published_date)", _connection);

                sqlCommand.Parameters.AddWithValue("@title", category.Title);
                sqlCommand.Parameters.AddWithValue("@subtitle", category.SubTitle);
                sqlCommand.Parameters.AddWithValue("@published", category.Published);
                sqlCommand.Parameters.AddWithValue("@publisher", category.Publisher);
                sqlCommand.Parameters.AddWithValue("@published_date", category.PublishedDate);

                int rows = sqlCommand.ExecuteNonQuery();

                return (rows > 0) ? category : null;

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public Article CreateArticle(Article article)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Insert Into [articles] ([title], [subtitle], [content], [creator], " +
                    "[created_date], [published], [publisher], [published_date]) " +
                    "Values (@title, @subtitle, @content, @creator, @created_date, @published, @publisher, @published_date) ", _connection);

                sqlCommand.Parameters.AddWithValue("@title", article.Title);
                sqlCommand.Parameters.AddWithValue("@subtitle", article.SubTitle);
                sqlCommand.Parameters.AddWithValue("@content", article.Content);
                sqlCommand.Parameters.AddWithValue("@creator", article.Creator);
                sqlCommand.Parameters.AddWithValue("@created_date", article.CreatedDate);
                sqlCommand.Parameters.AddWithValue("@published", article.Published);
                sqlCommand.Parameters.AddWithValue("@publisher", article.Publisher);
                sqlCommand.Parameters.AddWithValue("@published_date", article.PublishedDate);


                return (sqlCommand.ExecuteNonQuery() > 0) ? article : null;

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public Article InsertArticleImages(Article article)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Insert Into [article_images] ([article_id], [article_thumb_image], [article_main_image]) " +
                    "Values (@article_id, @article_thumb_image, @article_main_image)", _connection);

                sqlCommand.Parameters.AddWithValue("@article_id", article.ID);
                sqlCommand.Parameters.AddWithValue("@article_thumb_image", article.ThumbImage);
                sqlCommand.Parameters.AddWithValue("@article_main_image", article.MainImage);

                return (sqlCommand.ExecuteNonQuery() > 0) ? article : null;

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public Article GetArticleID(string article_title)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select [article_id] From [articles] Where [title] = @article_title", _connection);

                sqlCommand.Parameters.AddWithValue("@article_title", article_title);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                Article article = new Article();

                if (dataReader.Read())
                {
                    article.ID = int.Parse(dataReader["article_id"].ToString());

                    dataReader.Close();
                    return article;
                } else
                {
                    dataReader.Close();
                    return null;
                }

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public bool CheckIfArticleTitleExists(string article_title)
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select [title] From [articles] Where [title] = @article_title", _connection);

                sqlCommand.Parameters.AddWithValue("@article_title", article_title);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                if (dataReader.Read())
                {
                    dataReader.Close();
                    return true;
                } else
                {
                    dataReader.Close();
                    return false;
                }

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        public List<Category> GetListOfCategories()
        {
            try
            {
                OpenConnection();

                SqlCommand sqlCommand = new SqlCommand("Select * From [categories]", _connection);

                SqlDataReader dataReader = sqlCommand.ExecuteReader();

                List<Category> listCategories = new List<Category>();

                while (dataReader.Read())
                {
                    Category category = new Category()
                    {
                        Title = dataReader["title"].ToString(),
                        SubTitle = dataReader["subtitle"].ToString(),
                        Approver_User = dataReader["approver_user"].ToString() ?? string.Empty,
                        ApprovedDate = (DBNull.Value.Equals(dataReader["approved_date"].ToString())) ? DateTime.Parse(dataReader["approved_date"].ToString()) : (DateTime?)null,
                        Published = dataReader["published"].ToString(),
                        Publisher = dataReader["publisher"].ToString(),
                        PublishedDate = DateTime.Parse(dataReader["published_date"].ToString()), 
                        Remover = dataReader["remover"].ToString() ?? string.Empty, 
                        RemovedDate = (DBNull.Value.Equals(dataReader["removed_date"].ToString())) ? DateTime.Parse(dataReader["removed_date"].ToString()) : (DateTime?)null
                    };

                    listCategories.Add(category);
                }

                if (listCategories.Count > 0)
                {
                    dataReader.Close();
                    return listCategories;
                } else
                {
                    dataReader.Close();
                    return null;
                }

            } catch (Exception)
            {
                throw;
            } finally
            {
                CloseConnection();
            }
        }

        private void OpenConnection()
        {
            if (_connection.State != ConnectionState.Open)
            {
                _connection.Open();
            }
        }

        private void CloseConnection()
        {
            if (_connection.State != ConnectionState.Closed)
            {
                _connection.Close();
            }
        }

        public string EncryptPassword(string pwd)
        {
            MD5 md5 = MD5.Create();
            byte[] pwdBytes = Encoding.ASCII.GetBytes(pwd);
            byte[] hash = md5.ComputeHash(pwdBytes);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}