﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApp_Ex3;

namespace WebApp_Ex3
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MsgToRed();
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            DAL dal = new DAL();

            if (!CheckFields(txtBoxUsername.Text, txtBoxPassword.Text))
            {
                errorMsg.InnerText = "Pf preencher todos os campos! ";
            } else if (!dal.CheckIfDbHasAdministrator())
            {
                errorMsg.InnerText = "Não existem administradores criados! ";
            } else
            {
                User u = new User()
                {
                    Username = txtBoxUsername.Text,
                    Password = dal.EncryptPassword(txtBoxPassword.Text)
                };

                if (dal.GetUser(u) == null)
                {
                    errorMsg.InnerText = "Login Incorrecto! ";
                }
                else
                {
                    if (dal.CheckUserAccountState(u) != null)
                    {
                        Session["username"] = txtBoxUsername.Text;

                        if (dal.CheckIfUserIsAdmin(u))
                        {
                            if (dal.CheckIfPendingAccountsExist() == null)
                            {
                                Response.Redirect("Products.aspx");
                            }
                            else
                            {
                                Response.Redirect("ManageAccounts.aspx");
                            }
                        } else
                        {
                            Response.Redirect("Products.aspx");
                        }
                    } else
                    {
                        errorMsg.InnerText = "Não tem permissões para fazer login! Pf contacte o Administrador. ";
                    }
                }
            }
        }

        public bool CheckFields(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return false;
            } else
            {
                return true;
            }
        }

        protected void LinkBtnCreateUser_Click(object sender, EventArgs e)
        {
            if (!CheckIfAdminExists())
            {
                bottomMsg.InnerText = "Não é possível criar novo utilizador porque não existem administradores criados! ";
            } else
            {
                Response.Redirect("CreateUser.aspx");
            }
        }

        protected bool CheckIfAdminExists()
        {
            DAL dal = new DAL();

            return dal.CheckIfDbHasAdministrator() ? true : false;
        }

        protected void MsgToRed()
        {
            errorMsg.Style.Clear();
            errorMsg.Style.Add("color", "red");
        }
    }
}