﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Products.aspx.cs" Inherits="WebApp_Ex3.Products" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button Text="Logout" OnClick="Logout_Click" runat="server" />

            <ul id="ulLinks">
                <li><asp:HyperLink ID="linkNewCategory" Text="Adicionar Nova Categoria" NavigateUrl="~/AddCategory.aspx" runat="server"></asp:HyperLink></li>
                <li><asp:HyperLink ID="linkNewArticle" Text="Adicionar Novo Artigo" NavigateUrl="~/AddArticle.aspx" runat="server"></asp:HyperLink></li>
            </ul>

            <h1>Lista de Artigos</h1>
        </div>
    </form>
</body>
</html>
